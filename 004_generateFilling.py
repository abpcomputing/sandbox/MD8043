import json                                    
import numpy as np                          
import pandas as pd
import matplotlib.pyplot as plt

#https://github.com/PyCOMPLETE/FillingPatterns       
import fillingpatterns as fp                            
import WireDAQ.SigmaEff as sEff

INJscheme = {}
INJscheme['beam1'] = pd.DataFrame({ 'RFBucket':[1,  2001,   7941,   13881,  19571,  25761],
                                    'Length'  :[12, 48,     1,      48,     1,      48]})

INJscheme['beam2'] = pd.DataFrame({ 'RFBucket':[301,    2001,   7941,   13881,  20071,  25761],
                                    'Length'  :[12,     48,     1,      48,     1,      48]})




# Make filling from INJScheme
filling_scheme = {}                                                                                  
for beam in ['beam1','beam2']:
    filling_scheme[beam] = np.zeros(3564, dtype=int)

    for bucket_s,train_l in zip(INJscheme[beam].RFBucket,INJscheme[beam].Length):
        # get bunch number
        bunch_s = bucket_s//10
        bunch_e = bunch_s + train_l

        filling_scheme[beam][bunch_s:bunch_e] = 1

    filling_scheme[beam] = filling_scheme[beam].tolist()
#========================================================================               
                    
with open('Utilities/MD8043_filling.json','w') as fid:                        
  json.dump(filling_scheme, fid)                         


#patt = fp.FillingPattern.from_json('MD8043_filling.json')                 
#patt.compute_beam_beam_schedule(n_lr_per_side=21) 
patt = sEff.getFillingPattern('Utilities/MD8043_filling.json')
# Clearing some warnings
#clear_output(wait=False)

# Showing relevant info
INJscheme['beam1'].insert(0,'Beam','Beam 1')
INJscheme['beam2'].insert(0,'Beam','Beam 2')
print(pd.concat([INJscheme['beam1'],INJscheme['beam2']],axis=1))

fig, axes = plt.subplots(figsize= (15,5),ncols=1, nrows=2,gridspec_kw={'height_ratios': [1, 1]})
for beam,bb_df,ax,color in zip(['b1','b2'],[patt.b1.bb_schedule,patt.b2.bb_schedule],axes,['C0','C3']):

    plt.sca(ax)
    plt.stem(bb_df.index,np.ones(len(bb_df.index)),markerfmt='none',basefmt='none',linefmt=color)

    plt.xlim([-15,3564])
    plt.ylim([0,1.2])
fig.suptitle('Filling Pattern')
plt.xlabel('Bunch number')
plt.tight_layout()
plt.savefig('Utilities/MD8043_filling.png',dpi=300,format='png')


fig, axes = plt.subplots(figsize= (15,5),ncols=1, nrows=2,gridspec_kw={'height_ratios': [1, 1]})
for beam,bb_df,ax,color in zip(['b1','b2'],[patt.b1.bb_schedule,patt.b2.bb_schedule],axes,['C0','C3']):

    plt.sca(ax)
    plt.plot(bb_df['# of LR in ATLAS/CMS'].values,'o',color=color)

    plt.xticks(np.arange(len(bb_df))[::4],[f"T{_bunch['Train']}, B{_bunch['Tag']}" for _idx,_bunch in bb_df.iterrows()][::4],rotation=90)
fig.suptitle('Number of LRs')
plt.xlabel('Train,Bunch')
plt.tight_layout()
plt.savefig('Utilities/MD8043_LRs.png',dpi=300,format='png')
