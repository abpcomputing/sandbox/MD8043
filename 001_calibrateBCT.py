#%%

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates
import pandas as pd
import glob
import yaml
import sys
from pathlib import Path

sys.path.append('../')


# Make sure this import is after pandas
import WireDAQ.PandasPlus

import WireDAQ.Constants as cst
import WireDAQ.NXCALS as nx
import WireDAQ.SigmaEff as sEff

import Utilities.plotter as MDplt


# Matplotlib config
#============================
for key in plt.rcParams.keys():
    if 'date.auto' in key:
        plt.rcParams[key] = "%H:%M"
#============================
        

# Creating NXCALS variable containers
#============================
wires     = {'B1': [nx.NXCALSWire(loc = loc) for loc in ['L1B1','L5B1']],
             'B2': [nx.NXCALSWire(loc = loc) for loc in ['R1B2','R5B2']]}
beams     = [nx.NXCALSBeam(name) for name in ['B1','B2']]
buckets   = np.arange(3564)
#============================

# Loading data
#============================
with open('config.yaml','r') as fid:
    configuration = yaml.safe_load(fid)

TZONE = configuration['time_zone']

# Finding last file and importing data
parquet_path = Path(configuration['parquet_path'])
filePool = list(parquet_path.glob('*'))
last_idx = pd.Series(filePool).apply(lambda line:pd.Timestamp(line.name.split('+')[0])).sort_values().index[-1]

database = sEff.import_MD_Data(filePool[last_idx])

# Taking subset defined in the config
if configuration['stop_ts'] == 'None':
    configuration['stop_ts'] = str(database['Timestamp'].max()).split('+')[0]

database = database[(pd.Timestamp(configuration['start_ts'],tz=TZONE)<database['Timestamp'])&
                    (pd.Timestamp(configuration['stop_ts'],tz=TZONE)>database['Timestamp'])]
#============================




# Computing intensity avg (every 20 seconds)ç
def calibrate_BCT(df,calib_ts):

    # Computing intensity avg (every 20 seconds)
    binWind = 20

    _times   = df.set_index('Time')[beams[0]['bb_Luminosity']['ATLAS']].dropna().index
    bins    = np.arange(_times[0]+binWind/2,_times[-1],binWind)

    calib_dict = {'B1':{'A':1.0,'B':1.0},'B2':{'A':1.0,'B':1.0}}
    for beam in beams:

        observable = beam['Intensity']
        
        times,ref  = df.bin(observable,bins=bins)[['Time',observable]].T.to_numpy()
        calib_time = df.at_ts(calib_ts,'Time',method='nearest')
        calib_idx  = np.argmin(np.abs(times-calib_time))

        observable = beam['bb_Intensity']
        total_A    = df.bin(observable,bins=bins)[observable]
        calib_A   = np.mean((ref/total_A.apply(lambda line:np.sum(line)))[calib_idx:calib_idx+int(10*(60/binWind))])

        observable = beam['bb_Intensity_B']
        total_B    = df.bin(observable,bins=bins)[observable]
        calib_B   = np.mean((ref/total_B.apply(lambda line:np.sum(line)))[calib_idx:calib_idx+int(10*(60/binWind))])

        calib_dict[beam.name]['A'] = calib_A
        calib_dict[beam.name]['B'] = calib_B

    print('copy starred section:\n\n')
    print(( f"#******************************\n"
            f"calib_ts  : {str(calib_ts).split('+')[0]}\n"
            f"B1_BCT_A  : {calib_dict['B1']['A']:.17f}\n"
            f"B1_BCT_B  : {calib_dict['B1']['B']:.17f}\n"
            f"B2_BCT_A  : {calib_dict['B2']['A']:.17f}\n"
            f"B2_BCT_B  : {calib_dict['B2']['B']:.17f}\n"
            f"#******************************"))




calibrate_BCT(database,pd.Timestamp(configuration['calib_ts'],tz=TZONE))
#database[database['HX:BMODE'] == 'STABLE']['Time'].iloc[0]

# %%
