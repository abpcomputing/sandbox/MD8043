# installing conda
mkdir ./Executables
wget -P ./Executables https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh 
bash ./Executables/Miniconda3-latest-Linux-x86_64.sh -b  -p ./Executables/miniconda -f


# create your own virtual environment in a new folder
source ./Executables/miniconda/bin/activate
python -m venv ./Executables/py_MD8043
source ./Executables/py_MD8043/bin/activate


# Install acc-py and NXCALS
python -m pip install git+https://gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config.git
python -m pip install --no-cache nxcals 


# Install generic python packages
#========================================
pip install jupyterlab
pip install ipywidgets
pip install PyYAML
pip install pyarrow
pip install pandas
pip install matplotlib
pip install scipy
pip install ipympl
pip install ruamel.yaml
pip install rich
pip install lfm
pip install pynaff
pip install pynumdiff
pip install dask

# Adding the jupyter kernel to the list of kernels
python -m ipykernel install --user --name py_MD8043 --display-name "py_MD8043"
#========================================


# Install CERN packages
#=========================================
pip install cpymad

git clone https://gitlab.cern.ch/lhclumi/lumi-followup.git ./Executables/py_MD8043/lumi-followup
pip install -e ./Executables/py_MD8043/lumi-followup/nx2pd

git clone https://github.com/lhcopt/lhcmask.git ./Executables/py_MD8043/lhcmask
pip install -e ./Executables/py_MD8043/lhcmask

git clone https://github.com/PyCOMPLETE/FillingPatterns.git ./Executables/py_MD8043/FillingPatterns
pip install -e ./Executables/py_MD8043/FillingPatterns
#=========================================

