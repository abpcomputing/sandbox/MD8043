# WATCH IF NEW FILE CREATED
import glob
import os
import yaml
import time
from pathlib import Path
import sys

with open('config.yaml','r') as fid:
    configuration = yaml.safe_load(fid)

TZONE = configuration['time_zone']

# Where to store the parquet files
my_path=Path(configuration['parquet_path'])


files = sorted(glob.glob(str(my_path / '*.parquet')))
if len(files) > 4:
    print('New file detected, running... in 20 sec')
    for file in files[:-4]:
        os.remove(file)
    time.sleep(20)
else:
    sys.exit('No new file created')
print('Running...')



# MAIN CODE BELOW
#================================================
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates
import pandas as pd
import glob
import yaml


sys.path.append('../')


# Make sure this import is after pandas
import WireDAQ.PandasPlus

import WireDAQ.Constants as cst
import WireDAQ.NXCALS as nx
import WireDAQ.SigmaEff as sEff

import Utilities.plotter as MDplt
import Utilities.checker as MDchecks
# Matplotlib config
#============================
for key in plt.rcParams.keys():
    if 'date.auto' in key:
        plt.rcParams[key] = "%H:%M"
#============================
    

# Creating NXCALS variable containers
#============================
wires     = {'B1': [nx.NXCALSWire(loc = loc) for loc in ['L1B1','L5B1']],
             'B2': [nx.NXCALSWire(loc = loc) for loc in ['R1B2','R5B2']]}
beams     = [nx.NXCALSBeam(name) for name in ['B1','B2']]
buckets   = np.arange(3564)
#============================



# Finding last file and importing data
parquet_path = Path(configuration['parquet_path'])
filePool = list(parquet_path.glob('*'))
last_idx = pd.Series(filePool).apply(lambda line:pd.Timestamp(line.name.split('+')[0])).sort_values().index[-1]

database = sEff.import_MD_Data(filePool[last_idx])

# Taking subset defined in the config
if configuration['stop_ts'] == 'None':
    configuration['stop_ts'] = str(database['Timestamp'].max()).split('+')[0]

database = database[(pd.Timestamp(configuration['start_ts'],tz=TZONE)<database['Timestamp'])&
                    (pd.Timestamp(configuration['stop_ts'],tz=TZONE)>database['Timestamp'])]
#============================


# Analysis
#============================

# Loading filling pattern with extra train info
patt = sEff.getFillingPattern('Utilities/MD8043_filling.json')


# Computing intensity avg (every 20 seconds) with calibration BCT_A+BCT_B
BCT_avg  = sEff.compute_BCT_avg(database,configuration)
database = pd.concat([database,BCT_avg])
database = database.sort_index()

# Computing Lumi tot (ATLAS + CMS bunch by bunch)
Lumi_tot = sEff.computeLumiTot(database)
database = pd.concat([database,Lumi_tot])
database = database.sort_index()



# Computing effective cross section, both raw and smoothed 
xsection_df        = {}
xsection_smooth_df = {}
for beam in beams:
    xsection_df[beam.name]         = sEff.compute_lifetime_xsection(database,beam)
    xsection_smooth_df[beam.name]  = sEff.smooth_lifetime_xsection(database,beam)

# SAVING
#==================================================================
ts_str = database['Timestamp'].max().strftime('%Y%m%d_%H_%M_%S')
def mkdir(_path):
    if not Path(_path).exists():
        Path(_path).mkdir(parents=True, exist_ok=True)



# Saving df for livePlotting
import os
import glob
for beam in beams:
    df_path = configuration['analysis_path'] + f'/{beam.name}/xsection/'
    mkdir(df_path)
    xsection_df[beam.name].to_pickle(f'{df_path}/xsection_{ts_str}.pkl')
    xsection_smooth_df[beam.name].to_pickle(f'{df_path}/xsection_smooth_{ts_str}.pkl')

    # Cleaning dataframes to keep only last 5
    files = sorted(glob.glob(df_path + f'/xsection_{ts_str[0]}*.pkl'))
    if len(files) > 5:
        for file in files[:-5]:
            os.remove(file)

    files = sorted(glob.glob(df_path + f'/xsection_smooth_{ts_str[0]}*.pkl'))
    if len(files) > 5:
        for file in files[:-5]:
            os.remove(file)


# Plotting and saving plots
DPI = 200
for beam in beams:

    fig_path = configuration['analysis_path'] + f'/{beam.name}/Figs_Raw/'
    mkdir(fig_path)
    MDplt.plot_000_wire(database,xsection_df,xsection_smooth_df,beam.name,patt,configuration)
    #MDplt.plot_000(xsection_df,xsection_smooth_df,beam.name,patt,configuration)
    plt.savefig(f'{fig_path}/Fig_{ts_str}.png',dpi=DPI,format='png')

    fig_path = configuration['analysis_path'] + f'/{beam.name}/Figs_Avg/'
    mkdir(fig_path)
    MDplt.plot_001_wire(database,xsection_df,xsection_smooth_df,beam.name,patt,configuration)
    #MDplt.plot_001(xsection_df,xsection_smooth_df,beam.name,patt,configuration)
    plt.savefig(f'{fig_path}/Fig_{ts_str}.png',dpi=DPI,format='png')


    fig_path = configuration['analysis_path'] + f'/{beam.name}/Tests/'
    mkdir(fig_path)
    plt.figure()
    database.nxPlot('Timestamp',beams[0]['Intensity'],color='C0')
    database.nxPlot('Timestamp',beams[1]['Intensity'],color='C3')
    plt.ylim([5e13,3.8e14])
    plt.axvline(database['Timestamp'].max())
    plt.savefig(f'{fig_path}/Fig_{ts_str}.png',dpi=DPI,format='png')


    fig_path = configuration['analysis_path'] + f'/Checks/'
    mkdir(fig_path)
    MDchecks.check_000(database,xsection_df,xsection_smooth_df,patt,configuration)
    plt.savefig(f'{fig_path}/Fig_{ts_str}.png',dpi=DPI,format='png')
    # plt.figure()
    # database.nxPlot('Timestamp',beams[0]['Intensity'],color='C0')
    # database.nxPlot('Timestamp',beams[1]['Intensity'],color='C3')
    # plt.ylim([5e13,3.8e14])
    # plt.axvline(database['Timestamp'].max())
    # plt.savefig(f'{fig_path}/Fig_{ts_str}.png',dpi=DPI,format='png')

