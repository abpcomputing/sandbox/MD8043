import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates
from matplotlib.legend_handler import HandlerTuple
import pandas as pd
import sys
sys.path.append('../')


# Make sure this import is after pandas
import WireDAQ.PandasPlus

import WireDAQ.Constants as cst
import WireDAQ.NXCALS as nx
import WireDAQ.SigmaEff as sEff


# Matplotlib config
for key in plt.rcParams.keys():
    if 'date.auto' in key:
        plt.rcParams[key] = "%H:%M"


# Creating NXCALS variable containers
#============================
wires     = {'B1': [nx.NXCALSWire(loc = loc) for loc in ['L1B1','L5B1']],
             'B2': [nx.NXCALSWire(loc = loc) for loc in ['R1B2','R5B2']]}
beams     = [nx.NXCALSBeam(name) for name in ['B1','B2']]
buckets   = np.arange(3564)
#============================





def plot_000(xsection_df,xsection_smooth_df,beamName,patt,configuration):
    # With the diff, the last 2 minutes are lost
    #last_time = database['Timestamp'].max() - pd.Timedelta(minutes=2)
    last_time = xsection_df[beamName]['Timestamp'].max()
    t0 = last_time - pd.Timedelta(minutes=configuration['prev_offset']+configuration['avg_window'])
    t1 = last_time - pd.Timedelta(minutes=configuration['prev_offset'])
    t2 = last_time - pd.Timedelta(minutes=configuration['avg_window'])
    t3 = last_time

    ylimits = [configuration['sig_lim_0'],configuration['sig_lim_1']]

    time_window = [t3 - pd.Timedelta(minutes=configuration['buffer']),t3 + pd.Timedelta(minutes=5)]

    for beam,bb_df in zip(beams,[patt.b1.bb_schedule,patt.b2.bb_schedule]):

        if beam.name != beamName.upper():
            continue

        # Choosing all bunches
        choosen_bunches = list(bb_df.index)
        
            
        fig, axes = plt.subplots(figsize=(12,24),ncols=1, nrows=7,gridspec_kw={'height_ratios': [1, 1,1,1,1,1,1]})
        
        # TOP PLOT
        #===============================================================================
        plt.sca(axes[0])

        plt.plot(xsection_df[beam.name].iloc[-1]['sig_eff'][choosen_bunches]/1e-3,'.',color='C0',alpha=0.8,label = 'Now')


        # Manual legend
        plt.plot([np.nan],[np.nan],'sk',ms=3,alpha=0.3,label = 'Number of LRs')
        plt.legend(loc='upper right')

        plt.axhline(80,ls='--',color='k')
        plt.ylim(ylimits)
        plt.xlabel('Filled Bunch number')
        plt.ylabel(r'$\sigma_{eff}$ (mb)')

        # ADDING  LR PATTERN IN BACKGROUND
        #==================================
        plt.gca().set_frame_on(False)
        ax2 = plt.gca().twinx()
        ax2.set_zorder(-1)
        ax2.get_yaxis().set_visible(False)

        BB_signature = np.nan*np.ones(len(buckets))
        BB_signature[bb_df.index] =  bb_df['# of LR in ATLAS/CMS'].values
        plt.plot(BB_signature[choosen_bunches],'sk',ms=3,alpha=0.3,zorder=-10)

        #===============================================================================

        
        # BOTTOM PLOT
        #==============================================================================
        

        for name,group in bb_df.groupby('Train'):
            plt.sca(axes[int(name+1)])
            tagged = group.index
   
            # Plotting all bunches
            for bunch in tagged:

                ts       = xsection_df[beam.name]['Timestamp']
                xsection = xsection_df[beam.name]['sig_eff'].apply(lambda line:line[bunch])
                lifetime = xsection_df[beam.name]['Lifetime'].apply(lambda line:line[bunch])


                plt.plot(ts       ,xsection/1e-3       ,'-',alpha=0.3,color='C0')
            
            plt.plot([np.nan],[np.nan],'-',alpha=0.3,color='C0',label=f'Group {name}')
            plt.axhline(80,ls='--',color='k')
            plt.ylim(ylimits)
            plt.xlim(time_window)
            plt.ylabel(r'$\sigma_{eff}$ (mb)')
            plt.legend()
            

            # Computing avg
            avg_ts              = xsection_smooth_df[beam.name]['Timestamp']
            avg_xsection_smooth = xsection_smooth_df[beam.name]['sig_eff'].apply(lambda line:np.mean(line[tagged]))
            plt.plot(avg_ts,avg_xsection_smooth/1e-3,'-',alpha=1,color='k')

    


        plt.xlabel(r'Timestamp')
        axes[0].set_title(f'MD8043, {beam.name} @ [{str(time_window[0])[11:19]} - {str(xsection_df[beam.name]["Timestamp"].iloc[-1])[11:19]}]')
        plt.tight_layout()

def plot_000_wire(database,xsection_df,xsection_smooth_df,beamName,patt,configuration):
    # With the diff, the last 2 minutes are lost
    #last_time = database['Timestamp'].max() - pd.Timedelta(minutes=2)
    last_time = xsection_df[beamName]['Timestamp'].max()
    t0 = last_time - pd.Timedelta(minutes=configuration['prev_offset']+configuration['avg_window'])
    t1 = last_time - pd.Timedelta(minutes=configuration['prev_offset'])
    t2 = last_time - pd.Timedelta(minutes=configuration['avg_window'])
    t3 = last_time

    ylimits = [configuration['sig_lim_0'],configuration['sig_lim_1']]

    time_window = [t3 - pd.Timedelta(minutes=configuration['buffer']),t3 + pd.Timedelta(minutes=5)]

    for beam,bb_df in zip(beams,[patt.b1.bb_schedule,patt.b2.bb_schedule]):

        if beam.name != beamName.upper():
            continue

        # Choosing all bunches
        choosen_bunches = list(bb_df.index)
        
            
        fig, axes = plt.subplots(figsize=(12,24),ncols=1, nrows=7,gridspec_kw={'height_ratios': [1, 1,1,1,1,1,1]})
        
        # TOP PLOT
        #===============================================================================
        plt.sca(axes[0])

        plt.plot(xsection_df[beam.name].iloc[-1]['sig_eff'][choosen_bunches]/1e-3,'.',color='C0',alpha=0.8,label = 'Now')


        # Manual legend
        plt.plot([np.nan],[np.nan],'sk',ms=3,alpha=0.3,label = 'Number of LRs')
        plt.legend(loc='upper right')

        plt.axhline(80,ls='--',color='k')
        plt.ylim(ylimits)
        plt.xlabel('Filled Bunch number')
        plt.ylabel(r'$\sigma_{eff}$ (mb)')

        # ADDING  LR PATTERN IN BACKGROUND
        #==================================
        plt.gca().set_frame_on(False)
        ax2 = plt.gca().twinx()
        ax2.set_zorder(-1)
        ax2.get_yaxis().set_visible(False)

        BB_signature = np.nan*np.ones(len(buckets))
        BB_signature[bb_df.index] =  bb_df['# of LR in ATLAS/CMS'].values
        plt.plot(BB_signature[choosen_bunches],'sk',ms=3,alpha=0.3,zorder=-10)

        #===============================================================================

        
        # BOTTOM PLOT
        #==============================================================================
        

        for name,group in bb_df.groupby('Train'):
            plt.sca(axes[int(name+1)])
            tagged = group.index
   
            # Plotting all bunches
            for bunch in tagged:

                ts       = xsection_df[beam.name]['Timestamp']
                xsection = xsection_df[beam.name]['sig_eff'].apply(lambda line:line[bunch])
                lifetime = xsection_df[beam.name]['Lifetime'].apply(lambda line:line[bunch])


                plt.plot(ts       ,xsection/1e-3       ,'-',alpha=0.3,color='C0')
            
            plt.plot([np.nan],[np.nan],'-',alpha=0.3,color='C0',label=f'Group {name}')
            plt.axhline(80,ls='--',color='k')
            plt.ylim(ylimits)
            plt.xlim(time_window)
            plt.ylabel(r'$\sigma_{eff}$ (mb)')
            plt.legend()
            

            # Computing avg
            avg_ts              = xsection_smooth_df[beam.name]['Timestamp']
            avg_xsection_smooth = xsection_smooth_df[beam.name]['sig_eff'].apply(lambda line:np.mean(line[tagged]))
            plt.plot(avg_ts,avg_xsection_smooth/1e-3,'-',alpha=1,color='k')

            # ADDING  WIRE IN BACKGROUND
            #==================================
            plt.gca().set_frame_on(False)
            ax2 = plt.gca().twinx()
            ax2.set_zorder(-1)
            #ax2.get_yaxis().set_visible(False)

            for wire in wires['B2']:
                database.nxPlot('Timestamp',wire.I,color='C1',alpha=0.2)
            plt.ylim([-5,380])


        plt.xlabel(r'Timestamp')
        axes[0].set_title(f'MD8043, {beam.name} @ [{str(time_window[0])[11:19]} - {str(xsection_df[beam.name]["Timestamp"].iloc[-1])[11:19]}]')
        plt.tight_layout()


def plot_001(xsection_df,xsection_smooth_df,beamName,patt,configuration):
    # With the diff, the last 2 minutes are lost
    #last_time = database['Timestamp'].max() - pd.Timedelta(minutes=2)
    last_time = xsection_df[beamName]['Timestamp'].max()
    t0 = last_time - pd.Timedelta(minutes=configuration['prev_offset']+configuration['avg_window'])
    t1 = last_time - pd.Timedelta(minutes=configuration['prev_offset'])
    t2 = last_time - pd.Timedelta(minutes=configuration['avg_window'])
    t3 = last_time

    ylimits = [configuration['sig_lim_0'],configuration['sig_lim_1']]

    time_window = [t3 - pd.Timedelta(minutes=configuration['buffer']),t3 + pd.Timedelta(minutes=5)]

    for beam,bb_df in zip(beams,[patt.b1.bb_schedule,patt.b2.bb_schedule]):

        if beam.name != beamName.upper():
            continue

        # Choosing all bunches
        choosen_bunches = list(bb_df.index)
        
            
        fig, axes = plt.subplots(figsize=(12,16),ncols=1, nrows=4,gridspec_kw={'height_ratios': [1, 1,1,1]})
        
        # TOP PLOT
        #===============================================================================
        plt.sca(axes[0])
        ROI_Before = xsection_df[beam.name].set_index('Timestamp').loc[ t0:t1]
        ROI_After  = xsection_df[beam.name].set_index('Timestamp').loc[ t2:t3]

        plt.plot(ROI_Before['sig_eff'].mean()[choosen_bunches]/1e-3,'o',color='C3',alpha=0.5,label = f'{configuration["avg_window"]} min AVG, {configuration["prev_offset"]} min. ago')
        plt.plot(ROI_After['sig_eff'].mean()[choosen_bunches]/1e-3,'o',color='C2',alpha=0.5,label = f'{configuration["avg_window"]} min AVG,  0 min. ago')


        plt.plot(xsection_df[beam.name].iloc[-1]['sig_eff'][choosen_bunches]/1e-3,'.',color='C0',alpha=0.5,label = 'Now')


        # Manual legend
        plt.plot([np.nan],[np.nan],'sk',ms=3,alpha=0.3,label = 'Number of LRs')
        plt.legend(loc='upper right')

        plt.axhline(80,ls='--',color='k')
        plt.ylim(ylimits)
        plt.xlabel('Filled Bunch number')
        plt.ylabel(r'$\sigma_{eff}$ (mb)')

        # ADDING  LR PATTERN IN BACKGROUND
        #==================================
        plt.gca().set_frame_on(False)
        ax2 = plt.gca().twinx()
        ax2.set_zorder(-1)
        ax2.get_yaxis().set_visible(False)

        BB_signature = np.nan*np.ones(len(buckets))
        BB_signature[bb_df.index] =  bb_df['# of LR in ATLAS/CMS'].values
        plt.plot(BB_signature[choosen_bunches],'sk',ms=3,alpha=0.3,zorder=-10)

        #===============================================================================

        
        # BOTTOM PLOT
        #==============================================================================
        

        # Selecting bunches to consider for avg and std
        firsts  =  list(bb_df[bb_df['Tag'].isin(['1/48','2/48','3/48','4/48','5/48'])].index)
        middles =  list(bb_df[bb_df['Tag'].isin(['22/48','23/48','24/48','25/48','26/48'])].index)
        lasts   =  list(bb_df[bb_df['Tag'].isin(['44/48','45/48','46/48','47/48','48/48'])].index)
    

        for tagged,thisAx,title in zip([firsts,middles,lasts],[axes[1],axes[2],axes[3]],['First','Middle','Last']):
            plt.sca(thisAx)
            # Computing raw
            avg_raw_ts = xsection_df[beam.name]['Timestamp']
            avg_raw    = xsection_df[beam.name]['sig_eff'].apply(lambda line:np.mean(line[tagged]))


            # Computing avg
            avg_ts              = xsection_smooth_df[beam.name]['Timestamp']
            avg_xsection_smooth = xsection_smooth_df[beam.name]['sig_eff'].apply(lambda line:np.mean(line[tagged]))

            # Computing std
            std_ts              = xsection_smooth_df[beam.name]['Timestamp']
            std_xsection_smooth = xsection_smooth_df[beam.name]['sig_eff'].apply(lambda line:np.std(line[tagged]))


            y0 = (avg_xsection_smooth)/1e-3
            y1 = (avg_xsection_smooth-std_xsection_smooth)/1e-3
            y2 = (avg_xsection_smooth+std_xsection_smooth)/1e-3
            x  = std_ts.loc[y1.dropna().index]

            color = 'C4'
            plt.fill_between(np.array(x),np.array(y1.dropna()),np.array(y2.dropna()),color=color,alpha=0.3)
            plt.plot(avg_ts,y0,color='k',ls = '-',lw=1,alpha=1)
            plt.plot(std_ts,y1,color='k',ls = '-',lw=0.5,alpha=0.5)
            plt.plot(std_ts,y2,color='k',ls = '-',lw=0.5,alpha=0.5)
            plt.plot(avg_raw_ts,avg_raw/1e-3,color='C3',ls = '-',lw=1,alpha=1,label=r'Raw $\sigma_{eff}$')

            plt.fill_between([t0,t1], 0, 1, color='C3', alpha=0.15, transform=plt.gca().get_xaxis_transform(),label = f'{configuration["avg_window"]} min AVG, {configuration["prev_offset"]} min. ago',zorder=-10)
            plt.fill_between([t2,t3], 0, 1, color='C2', alpha=0.15, transform=plt.gca().get_xaxis_transform(),label = f'{configuration["avg_window"]} min AVG,  0 min. ago',zorder=-10)

            # LEGEND
            #=====================================
            plt.legend()
            handles, labels = plt.gca().get_legend_handles_labels()
            line0, = plt.plot(avg_ts,np.nan*y0,color='k',ls = '-',lw=1,alpha=1)
            line1 =  plt.fill_between([np.nan],[np.nan],[np.nan],color=color,alpha=0.3)

            plt.legend([(line0,line1)]+handles, 
                        [r'Smoothed $\sigma_{eff}$']+labels, handler_map = {tuple : HandlerTuple()},
                        title=f'{title} 5 bunches, each train')
            #====================================


            plt.axhline(80,ls='--',color='k')
            plt.ylim(ylimits)
            plt.xlim(time_window)
            plt.ylabel(r'$\sigma_{eff}$ (mb)')
            plt.xlabel(r'Timestamp')



        axes[0].set_title(f'MD8043, {beam.name} @ [{str(time_window[0])[11:19]} - {str(xsection_df[beam.name]["Timestamp"].iloc[-1])[11:19]}]')
        plt.tight_layout()




def plot_001_wire(database,xsection_df,xsection_smooth_df,beamName,patt,configuration):
    # With the diff, the last 2 minutes are lost
    #last_time = database['Timestamp'].max() - pd.Timedelta(minutes=2)
    last_time = xsection_df[beamName]['Timestamp'].max()
    t0 = last_time - pd.Timedelta(minutes=configuration['prev_offset']+configuration['avg_window'])
    t1 = last_time - pd.Timedelta(minutes=configuration['prev_offset'])
    t2 = last_time - pd.Timedelta(minutes=configuration['avg_window'])
    t3 = last_time

    ylimits = [configuration['sig_lim_0'],configuration['sig_lim_1']]

    time_window = [t3 - pd.Timedelta(minutes=configuration['buffer']),t3 + pd.Timedelta(minutes=5)]

    for beam,bb_df in zip(beams,[patt.b1.bb_schedule,patt.b2.bb_schedule]):

        if beam.name != beamName.upper():
            continue

        # Choosing all bunches
        choosen_bunches = list(bb_df.index)
        
            
        fig, axes = plt.subplots(figsize=(12,16),ncols=1, nrows=4,gridspec_kw={'height_ratios': [1, 1,1,1]})
        
        # TOP PLOT
        #===============================================================================
        plt.sca(axes[0])
        ROI_Before = xsection_df[beam.name].set_index('Timestamp').loc[ t0:t1]
        ROI_After  = xsection_df[beam.name].set_index('Timestamp').loc[ t2:t3]

        plt.plot(ROI_Before['sig_eff'].mean()[choosen_bunches]/1e-3,'o',color='C3',alpha=0.5,label = f'{configuration["avg_window"]} min AVG, {configuration["prev_offset"]} min. ago')
        plt.plot(ROI_After['sig_eff'].mean()[choosen_bunches]/1e-3,'o',color='C2',alpha=0.5,label = f'{configuration["avg_window"]} min AVG,  0 min. ago')


        plt.plot(xsection_df[beam.name].iloc[-1]['sig_eff'][choosen_bunches]/1e-3,'.',color='C0',alpha=0.5,label = 'Now')


        # Manual legend
        plt.plot([np.nan],[np.nan],'sk',ms=3,alpha=0.3,label = 'Number of LRs')
        plt.legend(loc='upper right')

        plt.axhline(80,ls='--',color='k')
        plt.ylim(ylimits)
        plt.xlabel('Filled Bunch number')
        plt.ylabel(r'$\sigma_{eff}$ (mb)')

        # ADDING  LR PATTERN IN BACKGROUND
        #==================================
        plt.gca().set_frame_on(False)
        ax2 = plt.gca().twinx()
        ax2.set_zorder(-1)
        ax2.get_yaxis().set_visible(False)

        BB_signature = np.nan*np.ones(len(buckets))
        BB_signature[bb_df.index] =  bb_df['# of LR in ATLAS/CMS'].values
        plt.plot(BB_signature[choosen_bunches],'sk',ms=3,alpha=0.3,zorder=-10)

        #===============================================================================

        
        # BOTTOM PLOT
        #==============================================================================
        

        # Selecting bunches to consider for avg and std
        firsts  =  list(bb_df[bb_df['Tag'].isin(['1/48','2/48','3/48','4/48','5/48'])].index)
        middles =  list(bb_df[bb_df['Tag'].isin(['22/48','23/48','24/48','25/48','26/48'])].index)
        lasts   =  list(bb_df[bb_df['Tag'].isin(['44/48','45/48','46/48','47/48','48/48'])].index)
    

        for tagged,thisAx,title in zip([firsts,middles,lasts],[axes[1],axes[2],axes[3]],['First','Middle','Last']):
            plt.sca(thisAx)
            # Computing raw
            avg_raw_ts = xsection_df[beam.name]['Timestamp']
            avg_raw    = xsection_df[beam.name]['sig_eff'].apply(lambda line:np.mean(line[tagged]))


            # Computing avg
            avg_ts              = xsection_smooth_df[beam.name]['Timestamp']
            avg_xsection_smooth = xsection_smooth_df[beam.name]['sig_eff'].apply(lambda line:np.mean(line[tagged]))

            # Computing std
            std_ts              = xsection_smooth_df[beam.name]['Timestamp']
            std_xsection_smooth = xsection_smooth_df[beam.name]['sig_eff'].apply(lambda line:np.std(line[tagged]))


            y0 = (avg_xsection_smooth)/1e-3
            y1 = (avg_xsection_smooth-std_xsection_smooth)/1e-3
            y2 = (avg_xsection_smooth+std_xsection_smooth)/1e-3
            x  = std_ts.loc[y1.dropna().index]

            color = 'C4'
            plt.fill_between(np.array(x),np.array(y1.dropna()),np.array(y2.dropna()),color=color,alpha=0.3)
            plt.plot(avg_ts,y0,color='k',ls = '-',lw=1,alpha=1)
            plt.plot(std_ts,y1,color='k',ls = '-',lw=0.5,alpha=0.5)
            plt.plot(std_ts,y2,color='k',ls = '-',lw=0.5,alpha=0.5)
            plt.plot(avg_raw_ts,avg_raw/1e-3,color='C3',ls = '-',lw=1,alpha=1,label=r'Raw $\sigma_{eff}$')

            plt.fill_between([t0,t1], 0, 1, color='C3', alpha=0.15, transform=plt.gca().get_xaxis_transform(),label = f'{configuration["avg_window"]} min AVG, {configuration["prev_offset"]} min. ago',zorder=-10)
            plt.fill_between([t2,t3], 0, 1, color='C2', alpha=0.15, transform=plt.gca().get_xaxis_transform(),label = f'{configuration["avg_window"]} min AVG,  0 min. ago',zorder=-10)

            # LEGEND
            #=====================================
            plt.legend()
            handles, labels = plt.gca().get_legend_handles_labels()
            line0, = plt.plot(avg_ts,np.nan*y0,color='k',ls = '-',lw=1,alpha=1)
            line1 =  plt.fill_between([np.nan],[np.nan],[np.nan],color=color,alpha=0.3)

            plt.legend([(line0,line1)]+handles, 
                        [r'Smoothed $\sigma_{eff}$']+labels, handler_map = {tuple : HandlerTuple()},
                        title=f'{title} 5 bunches, each train')
            #====================================


            plt.axhline(80,ls='--',color='k')
            plt.ylim(ylimits)
            plt.xlim(time_window)
            plt.ylabel(r'$\sigma_{eff}$ (mb)')
            plt.xlabel(r'Timestamp')

            # ADDING  WIRE IN BACKGROUND
            #==================================
            plt.gca().set_frame_on(False)
            ax2 = plt.gca().twinx()
            ax2.set_zorder(-1)
            #ax2.get_yaxis().set_visible(False)

            for wire in wires['B2']:
                database.nxPlot('Timestamp',wire.I,color='C1',alpha=0.2)
            plt.ylim([-5,380])



        axes[0].set_title(f'MD8043, {beam.name} @ [{str(time_window[0])[11:19]} - {str(xsection_df[beam.name]["Timestamp"].iloc[-1])[11:19]}]')
        plt.tight_layout()
