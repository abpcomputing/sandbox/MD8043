# Setup
1. Log into `lumimod@pcbe-abp-hpc002`
2. Install the project and activate python environement
```bash
git clone ssh://git@gitlab.cern.ch:7999/abpcomputing/sandbox/MD8043.git
cd MD8043
bash make_it.sh
source Executables/py_MD8043/bin/activate
```
3. Edit paths in the `config.yaml` file
4. Prepare `tmux` terminal with multiple tabs
```bash
# Tab 1, acquire data
# watch -n [seconds] python 000_onlineSpark.py
watch -n 240 python 000_onlineSpark.py

# Tab 2, monitor
tail -50f monitoring.log

# tab 3, after first download, choose calib_ts in config file and then
python 001_calibrateBCT.py  

# tab 4, plotting script
# watch -n [seconds] python 002_onlineSigmaEff.py
watch -n 10 python 002_plotWatcher.py
```



## Data
Data is stored under:
```bash
/eos/project/l/lhc-lumimod/MD8043/Online
```

## Logbook

## Check of the ObsBox
Verify from a TN machine (e.g. cs-ccr-dev1)  that the folder is updating
```
cd /nfs/cfc-sr4-adtobs2buf/obsbox/slow/B1H_Q7
find -cmin -1
```
 
