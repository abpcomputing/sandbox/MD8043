import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mpldates
from matplotlib.legend_handler import HandlerTuple
import pandas as pd
import sys
sys.path.append('../')


# Make sure this import is after pandas
import WireDAQ.PandasPlus

import WireDAQ.Constants as cst
import WireDAQ.NXCALS as nx
import WireDAQ.SigmaEff as sEff


# Matplotlib config
for key in plt.rcParams.keys():
    if 'date.auto' in key:
        plt.rcParams[key] = "%H:%M"


# Creating NXCALS variable containers
#============================
wires     = {'B1': [nx.NXCALSWire(loc = loc) for loc in ['L1B1','L5B1']],
             'B2': [nx.NXCALSWire(loc = loc) for loc in ['R1B2','R5B2']]}
beams     = [nx.NXCALSBeam(name) for name in ['B1','B2']]
buckets   = np.arange(3564)
#============================


def check_000(database,xsection_df,xsection_smooth_df,patt,configuration):

    # PLOTTING
    #--------------------------------------------------------
    start_ts = database['Timestamp'].max()

    fig, axes = plt.subplots(figsize=(12,6),ncols=1, nrows=2,gridspec_kw={'height_ratios': [1, 1]})

    for beam,ax in zip(beams,axes):

        compare_observables = [ beam['bb_Luminosity']['ATLAS'],
                                beam['bb_Luminosity']['CMS'],
                                f'bb_Intensity_A_{beam.name}',
                                f'bb_Intensity_B_{beam.name}',
                                f'bb_Intensity_avg_{beam.name}']


        plt.sca(ax)
        min_bucket = 0
        max_bucket = 100
        for observable in compare_observables:
            data = database.at_ts(start_ts ,observable,method='nearest',return_ts=False)
            plt.plot(buckets,data/np.max(data[min_bucket:max_bucket]),'-o',alpha=0.5,label=observable)

        plt.legend()
        plt.xlim([min_bucket,max_bucket])
        plt.ylim([0,1.1])
    plt.tight_layout()


    